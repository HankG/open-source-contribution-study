# Elastic & Kibana

## Creation Commands

Run the tool in archive generation mode on the [SerenityOS repository](https://github.com/SerenityOS/serenity)
to generate the JSON file used for analysis.

```bash
java -jar git-stat.jar load-repo -s outputs/serenityos_archive.json -b master serenity
```

## Data Generation Commands

Generate the statistics with these settings
```bash
python3 generate.py git-stat.jar projects/serenityos/inputs/serenityos_settings.json
```
