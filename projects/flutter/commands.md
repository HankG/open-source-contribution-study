# Elastic & Kibana

## Creation Commands

Run the tool in archive generation mode on the [Flutter repository](https://github.com/flutter/flutter)
to generate the JSON file used for analysis.

```bash
java -jar git-stat.jar load-repo -s outputs/flutter_archive.json -b master flutter
```

## Command to get list of tags

In each of the repositories get a list of the tag times to get approximate begin/end dates for various versions:

```bash
git tag --sort=creatordate --format='%(creatordate:iso-strict), %(refname:strip=2)'
```


## Data Generation Commands

Generate the statistics with these settings
```bash
python3 generate.py git-stat.jar projects/flutter/inputs/flutter_settings.json
```
