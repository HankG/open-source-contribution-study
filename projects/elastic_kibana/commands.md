# Elastic & Kibana

## Creation Commands

Run the tool in archive generation mode on Elastic and Kibana repositories to generate the JSON file used for analysis.

```bash
java -jar git-stat.jar load-repo -s outputs/elastic_search.json -b master elasticsearch
java -jar git-stat.jar load-repo -s outputs/kibana.json -b master kibana
```

## Command to get list of tags

In each of the repositories get a list of the tag times to get approximate begin/end dates for various versions:

```bash
git tag --sort=creatordate --format='%(creatordate:iso-strict), %(refname:strip=2)'
```


## Data Generation Commands

Generate the Kibana statistics with these settings
```bash
python3 generate.py git-stat.jar projects/elastic_kibana/inputs/kibana_settings.json
```

Generate the Elastic Search statistics with these settings:
```bash
python3 generate.py git-stat.jar projects/elastic_kibana/inputs/elastic_search_settings.json
```

## Gource visualizations commands

### Elastic

Generate All Orgs All Files
```bash
time gource --load-config open-source-contribution-study/projects/elastic_kibana/inputs/elastic_gource_full_project_all_orgs_all_files.config -f -o - | ffmpeg -y -r 60 -f image2pipe -vcodec ppm -i - -vcodec libx264 -preset ultrafast -pix_fmt yuv420p -crf 10 -threads 0 -bf 0 raw_video/elastic_all_dev_all_orgs.mp4
```

Generate Not Elastic Orgs All Files
```bash
time gource --load-config open-source-contribution-study/projects/elastic_kibana/inputs/elastic_gource_full_project_not_elastic_all_files.config -f -o - | ffmpeg -y -r 60 -f image2pipe -vcodec ppm -i - -vcodec libx264 -preset ultrafast -pix_fmt yuv420p -crf 10 -threads 0 -bf 0 raw_video/elastic_all_dev_not_elastic.mp4
```

Generate All Orgs Active Files
```bash
time gource --load-config open-source-contribution-study/projects/elastic_kibana/inputs/elastic_gource_full_project_all_orgs_active_files.config -f -o - | ffmpeg -y -r 60 -f image2pipe -vcodec ppm -i - -vcodec libx264 -preset ultrafast -pix_fmt yuv420p -crf 10 -threads 0 -bf 0 raw_video/elastic_active_dev_all_orgs.mp4
```

Generate Not Elastic Active Files
```bash
time gource --load-config open-source-contribution-study/projects/elastic_kibana/inputs/elastic_gource_full_project_not_elastic_active_files.config -f -o - | ffmpeg -y -r 60 -f image2pipe -vcodec ppm -i - -vcodec libx264 -preset ultrafast -pix_fmt yuv420p -crf 10 -threads 0 -bf 0 raw_video/elastic_active_dev_not_elastic.mp4
```

Generate All Users Active Files
```bash
time gource --load-config open-source-contribution-study/projects/elastic_kibana/inputs/elastic_gource_full_project_all_users_active_files.config -f -o - | ffmpeg -y -r 60 -f image2pipe -vcodec ppm -i - -vcodec libx264 -preset ultrafast -pix_fmt yuv420p -crf 10 -threads 0 -bf 0 raw_video/elastic_active_dev_all_users.mp4
```


###Kibana

Gource All Orgs All Files
```bash
time gource --load-config open-source-contribution-study/projects/elastic_kibana/inputs/kibana_gource_full_project_all_orgs_all_files.config -f -o - | ffmpeg -y -r 60 -f image2pipe -vcodec ppm -i - -vcodec libx264 -preset ultrafast -pix_fmt yuv420p -crf 10 -threads 0 -bf 0 raw_video/kibana_all_dev_all_orgs.mp4
```

Gource Non Elastic Orgs All Files
```bash
time gource --load-config open-source-contribution-study/projects/elastic_kibana/inputs/kibana_gource_full_project_not_elastic_all_files.config -f -o - | ffmpeg -y -r 60 -f image2pipe -vcodec ppm -i - -vcodec libx264 -preset ultrafast -pix_fmt yuv420p -crf 10 -threads 0 -bf 0 raw_video/kibana_all_dev_not_elastic.mp4
```

Gource All Orgs Active Files
```bash
time gource --load-config open-source-contribution-study/projects/elastic_kibana/inputs/kibana_gource_full_project_all_orgs_active_files.config -f -o - | ffmpeg -y -r 60 -f image2pipe -vcodec ppm -i - -vcodec libx264 -preset ultrafast -pix_fmt yuv420p -crf 10 -threads 0 -bf 0 raw_video/kibana_active_dev_all_orgs.mp4
```

Gource Not Elastic Orgs Active Files
```bash
time gource --load-config open-source-contribution-study/projects/elastic_kibana/inputs/kibana_gource_full_project_not_elastic_active_files.config -f -o - | ffmpeg -y -r 60 -f image2pipe -vcodec ppm -i - -vcodec libx264 -preset ultrafast -pix_fmt yuv420p -crf 10 -threads 0 -bf 0 raw_video/kibana_active_dev_not_elastic.mp4
```

Gource All Users Active Files
```bash
time gource --load-config open-source-contribution-study/projects/elastic_kibana/inputs/kibana_gource_full_project_all_users_active_files.config -f -o - | ffmpeg -y -r 60 -f image2pipe -vcodec ppm -i - -vcodec libx264 -preset ultrafast -pix_fmt yuv420p -crf 10 -threads 0 -bf 0 raw_video/kibana_active_dev_all_users.mp4
```
