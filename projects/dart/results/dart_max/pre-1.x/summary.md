## Dart Pre-1.x


![Total Commits Per Month](total_commits_per_month.png "Total Commits Per Month")

[Total Commits Per Month CSV Data ](total_commits_per_month.csv)


![Total Files Per Month](total_files_per_month.png "Total Files Per Month")

[Total Files Per Month CSV Data ](total_files_per_month.csv)


![Total Lines Per Month](total_lines_per_month.png "Total Lines Per Month")

[Total Lines Per Month CSV Data ](total_lines_per_month.csv)


![Total Line Volume Per Month](total_linevolume_per_month.png "Total Line Volume Per Month")

[Total Commits Per Line Volume CSV Data ](total_linevolume_per_month.csv)


![File Count](file_count.png "File Count")

[File Count CSV Data ](file_count.csv)


![Line Count](line_count.png "Line Count")

[Line Count CSV Data ](line_count.csv)


![Commits History By Users](commits_history_by_users.png "Commits History by Users")

[Commits History By Users CSV Data ](commits_history_by_users.csv)


![Commits History Percent By Users](commits_history_by_users_pct.png "Commits History Percent by Users")

[Commits History Percent By Users CSV Data ](commits_history_by_users_pct.csv)


![Top Contributing Organizations](top_contrib_orgs.png "Top Contributing Organizations")

[Top Contributing Organizations CSV Data ](top_contrib_orgs.csv)


![Top Contributing Orgs Commit History](top_contrib_orgs_commit_history.png "Top Contributing Orgs Commit History")

[Top Contributing Orgs Commit History CSV Data ](/top_contrib_orgs_commit_history.csv)


![Top Contributing Orgs Commit History Percent](rtop_contrib_orgs_commit_history_pct.png "Top Contributing Orgs Commit History Percent")

[Top Contributing Orgs Commit History Percent CSV Data ](top_contrib_orgs_commit_history_pct.csv)


![Top Contributing Orgs Line Volume History](top_contrib_orgs_linevolume_history.png "Top Contributing Orgs Line Volume History")

[Top Contributing Orgs Line Volume History CSV Data ](top_contrib_orgs_linevolume_history.csv)


![Top Contributing Orgs Line Volume History Percent](top_contrib_orgs_linevolume_history_pct.png "Top Contributing Orgs Line Volume History Percent")

[Top Contributing Orgs Line Volume History Percent CSV Data ](top_contrib_orgs_linevolume_history_pct.csv)


![Top Contributing Users Line Volume History](top_linevolume_by_user.png "Top Contributing Users Line Volume History")

[Top Contributing Users Line Volume History CSV Data ](top_linevolume_by_user.csv)

![Top Contributing Users Line Volume History Percent](top_linevolume_by_user_pct.png "Top Contributing Users Line Volume History Percent")

[Top Contributing Users Line Volume History Percent CSV Data ](top_linevolume_by_user_pct.csv)
