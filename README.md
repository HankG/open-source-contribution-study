# Open Source Contribution Study

A collection of statistics on the software contributions of various open source projects as measured by information available in their Git repositories. 